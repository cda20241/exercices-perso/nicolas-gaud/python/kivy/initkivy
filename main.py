from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
Window.size = (800, 600)

class MyApp(App):

    def __init__(self, **kwargs):
        super().__init__()
        self.rootLayout = None

        self.topGridLayout = None
        self.bottomGRidLayout = None

        self.textInput = None
        self.resLabel = None

        self.button1 = None
        self.button2 = None
        self.button3 = None
        self.button4 = None
        self.button5 = None
        self.button6 = None

    def build(self):
        self.rootLayout = BoxLayout(orientation='vertical')

        self.topGridLayout = GridLayout(rows=1, cols=2, row_force_default=True, row_default_height=100)
        self.textInput = TextInput(text='', size_hint=(.5, 1))
        self.textInput.bind(text=self.on_text)
        self.topGridLayout.add_widget(self.textInput)
        self.resLabel = Label(text='Entrez du texte', size_hint=(.5, 1))
        self.topGridLayout.add_widget(self.resLabel)

        self.bottomGRidLayout = GridLayout(rows=3, cols=2, row_force_default=True, row_default_height=100)
        self.button1 = Button(text='Rouge', background_color='red', background_normal='')
        self.button1.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button1)

        self.button2 = Button(text='Bleu', background_color='blue', background_normal='')
        self.button2.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button2)

        self.button3 = Button(text='Vert', background_color='green', background_normal='')
        self.button3.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button3)

        self.button4 = Button(text='Jaune', background_color='yellow', background_normal='')
        self.button4.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button4)

        self.button5 = Button(text='Rose', background_color='pink', background_normal='')
        self.button5.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button5)

        self.button6 = Button(text='Marron', background_color='brown', background_normal='')
        self.button6.bind(on_press=self.on_press)
        self.bottomGRidLayout.add_widget(self.button6)

        self.rootLayout.add_widget(self.topGridLayout)
        self.rootLayout.add_widget(self.bottomGRidLayout)

        return self.rootLayout

    def on_text(self, instance, value):
        self.resLabel.text = value

    def on_press(self, button):
        self.resLabel.color = button.background_color


if __name__ == '__main__':
    MyApp().run()
